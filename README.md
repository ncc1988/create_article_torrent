# create_article_torrent

create_article_torrent is a plugin for the Pelican static site generator to allow sharing articles via the BitTorrent network.

## Prerequisites

- Pelican (of course)
- mktorrent

## How it works

It is invoked on the the content_written signal (to have a fully generated page
ready) and generates a torrent file for each article. If an article already has
a torrent file, it isn't recreated.

Torrent files are created using the mktorrent executable of the
operating system. The torrent file name is equal to the output path of the
article with the extention ".torrent" added to it.

The article file generated by Pelican is also added as web seed URL to the
torrent to help "spreading the news" directly after publishing the article.

## Settings

### TORRENT_TRACKER_URL

You can specify the URL of a torrent tracker in your pelicanconf.py. The tracker
URL will be added to the torrent file. Example:

    TORRENT_TRACKER_URL = 'https://example.org'
