# create_article_torrent.py
# Copyright (C) 2021 Moritz Strohm <ncc1988@posteo.de>
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License along
# with this program. If not, see <https://www.gnu.org/licenses/>.


from pelican import signals, contents
import os, subprocess


def createTorrentFile(article_generator, context):
    article = next(iter(context['generated_content'].values()))
    if not isinstance(article, contents.Article):
        return
    #content = article
    if not article.save_as:
        print('Article ' + article.slug + ' has an empty file name. Cannot create a torrent file for it!')
        return

    article_path = article.settings['OUTPUT_PATH']
    if not article_path.endswith('/'):
        article_path += '/'
    article_path += article.save_as
    torrent_file_name = article_path + '.torrent'

    if (os.path.isfile(torrent_file_name)):
        return

    torrent_web_seed = article.settings['SITEURL']
    if not torrent_web_seed.endswith('/'):
        torrent_web_seed += '/'
    torrent_web_seed += article.save_as

    #Call mktorrent:
    args = ['mktorrent']
    if 'TORRENT_TRACKER_URL' in article.settings:
        if article.settings['TORRENT_TRACKER_URL']:
            args += ['-a', article.settings['TORRENT_TRACKER_URL']]

    args += ['-n', article.save_as, '-w', torrent_web_seed,
             '-o', torrent_file_name,
             article_path]

    mktorrent = subprocess.Popen(args)
    mktorrent.wait()
    if (mktorrent.returncode != 0):
        print('Article ' + article.slug + ': mktorrent sent an error code: ' + str(mktorrent.returncode))


def register():
    signals.content_written.connect(createTorrentFile)
